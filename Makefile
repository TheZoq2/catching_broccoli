SHELL=/bin/sh -o pipefail
$(shell cd spade && cargo build)

SPADEC = spade/target/debug/spade

SPADE_SOURCES=$(wildcard src/*.spade) $(wildcard spade/stdlib/*.spade)

TOP=src/top.v
VERILOG_SOURCES=build/main.v src/hacks.v

all: build/main.v

sim: build/main.v.vcd

# Build verilog from the spade code
build/main.v: $(SPADE_SOURCES) $(SPADEC) Makefile .PHONY
	@mkdir -p ${@D}
	@echo -e "[\033[0;34m${SPADEC}\033[0m] building $@"
	${SPADEC} $(SPADE_SOURCES) -o $@

test: build/main.v.vcd

# Build a test binary
build/main.v.out: build/main.v src/testbench.v
	@echo -e "[\033[0;34miverilog\033[0m] building $@"
	@iverilog \
		-o ${@} \
		${IVERILOG_FLAGS} \
		-grelative-include \
		-g2012 \
		-DVCD_OUTPUT=\"build/${<F}.vcd\" \
		$(VERILOG_SOURCES) src/testbench.v

# Simulate the test binary
build/main.v.vcd: build/main.v.out
	@mkdir -p output
	@echo -e "[\033[0;34mvvp\033[0m] simulating $@"
	@vvp $< | grep -v dumpfile


# ECPIX5 stuff
IDCODE ?= 0x81112043 # 12f

ecp5.json: build/main.v Makefile
	echo $(VERILOG_SOURCES)
	yosys \
		-p "read_verilog -sv $(VERILOG_SOURCES) $(TOP); synth_ecp5 -top top -json $@" \
		-E .$(basename $@).d

ecp5.config: ecp5.json ecpix5.lpf
	nextpnr-ecp5 \
		--json $< \
		--textcfg $@ \
		--lpf ecpix5.lpf \
		--um5g-45k \
		--package CABGA554

ecp5.bit: ecp5.config ecpix5.lpf
	ecppack --idcode $(IDCODE) $< $@

ecp5.svf: ecp5.config ecpix5.lpf
	ecppack --idcode $(IDCODE) --input $< --svf $@

upload: ecp5.svf .PHONY
	openocd -f openocd-ecpix5.cfg -c "init" -c "svf -quiet ecp5.svf" -c "exit"

clean:
	rm -rf build/

.SECONDARY: $(patsubst %, build/main.v, ${TEST_DIRS})
.PHONY: all

