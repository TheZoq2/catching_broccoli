use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Player {
    pub pos: (i32, i32),
    pub vel: (i32, i32),
}

#[derive(Serialize, Deserialize, Debug)]
pub enum InputKind {
    Press,
    Release
}

impl InputKind {
    pub fn byte_encoding(&self) -> u8 {
        match self {
            InputKind::Press => 1,
            InputKind::Release => 0,
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Input {
    pub key: u8,
    pub kind: InputKind
}
