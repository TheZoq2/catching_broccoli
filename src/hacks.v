module to_bytes24(input[23:0] _i_val, output[23:0] __output);
    assign __output = _i_val;
endmodule

module i8toi24(input[7:0] _i_val, output[23:0] __output);
    assign __output = {{16{_i_val[7]}}, _i_val};
endmodule
