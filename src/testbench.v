`include "vatch/main.v"

module main_tb();
    `SETUP_TEST

    reg clk;

    initial begin
        $dumpfile(`VCD_OUTPUT);
        $dumpvars(0, main_tb);
        clk = 1;
        forever begin
            clk = ~clk;
            #1;
        end
    end

    reg rst;
    reg[16:0] input_vals;

    initial begin
        rst = 1;
        @(negedge clk)
        @(negedge clk)
        @(negedge clk)
        rst = 0;


        #1_000_000


        `END_TEST
    end

    reg result;
    wire rx;

    main main
        ( ._i_clk(clk)
        , ._i_rst(rst)
        , ._i_rx_unsync(rx)
        , .__output(result)
        );



    initial begin
        input_vals = {1'b1, 8'b0, 8'b0};
        #10

        @(negedge clk)
        input_vals = {1'b0, 8'd87, 8'b1};
        @(negedge clk)
        input_vals = {1'b1, 8'b0, 8'b0};
    end

    wire[4:0] key_array;
    key_manager key_man
        ( ._i_clk(clk)
        , ._i_rst(rst)
        , ._i_input(input_vals)
        , .__output(key_array)
        );
endmodule

