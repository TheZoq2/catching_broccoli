// look in pins.pcf for all the pin names on the TinyFPGA BX board
module top (
    input clk,

    input rx,
    output tx
);
    reg rst = 1;
    always @(posedge clk) begin
        rst <= 0;
    end

    main main
        ( ._i_clk(clk)
        , ._i_rst(rst)
        , ._i_rx_unsync(rx)
        , .__output(tx)
        );
endmodule

