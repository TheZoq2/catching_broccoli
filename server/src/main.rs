use std::pin::Pin;
use std::time::Duration;

use anyhow::{Context, Result};
use clap::Parser;
use futures_util::{SinkExt, StreamExt, TryStreamExt};
use tokio::net::{TcpListener, TcpStream};
use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    sync::{mpsc, watch},
};
use tokio_serial::SerialPortBuilderExt;
use tokio_serial::SerialStream;
use tokio_tungstenite::{tungstenite::Message, WebSocketStream};

use serde::Serialize;

use libbroccoli::{Input, Player};


use log::{error, info};

use crate::metaverse::metaverse_manager;

mod metaverse;


#[derive(Parser, Debug)]
struct Args {
    #[clap(long, default_value_t = String::from("0.0.0.0"))]
    address: String,

    #[clap(long, default_value_t = 8080)]
    port: u32,

    #[clap(long)]
    meta_address: Option<String>,

    #[clap(long = "fake")]
    fake_server: bool,
}

#[tokio::main]
async fn main() -> Result<()> {
    let args = Args::parse();

    env_logger::builder()
        .filter_level(log::LevelFilter::Info)
        .try_init()
        .unwrap();



    let mut port = if args.fake_server {
        let (tx, rx) = SerialStream::pair()?;
        tokio::spawn(fake_pga(tx));
        rx
    } else {
        tokio_serial::new(
            "/dev/serial/by-id/usb-FTDI_Dual_RS232-HS-if01-port0",
            115200,
        )
        .open_native_async()
        .context("Failed to open serial port")?
    };

    let addr = format!("{}:{}", args.address, args.port);

    // Create the event loop and TCP listener we'll accept connections on.
    let try_socket = TcpListener::bind(&addr).await;
    let listener = try_socket.expect("Failed to bind");
    info!("Listening on: {}", addr);

    let (state_tx, state_rx) = watch::channel(None);
    let (input_tx, mut input_rx) = mpsc::channel(100);

    if let Some(meta_address) = args.meta_address {
        tokio::spawn(metaverse_manager(format!("http://{meta_address}"), state_rx.clone()));
    }

    let mut next_player_id = 0;
    loop {
        let mut buf = [0u8; 32];
        tokio::select! {
            result = port.read(&mut buf[..]) => {
                match result {
                    Ok(_len) => {
                        if _len >= 14 && buf[0] == 1 && buf[1] == 1{
                            let player = decode_player(&buf);
                            info!("{player:?}");
                            let player_json = serde_json::to_string(&player).unwrap();
                            state_tx.send(Some(player_json))?;
                        }
                    }
                    Err(e) => return Err(e).context("Failed to read serial port")
                }
            }
            Ok((stream, _)) = listener.accept() => {
                tokio::spawn(accept_connection(stream, state_rx.clone(), next_player_id, input_tx.clone()));
                next_player_id = next_player_id + 1;
            }
            encoded = input_rx.recv() => {
                match encoded {
                    Some(encoded) => {
                        port.write_all(&encoded).await.expect("Failed to send to serial");
                    }
                    None => {}
                }
            }
        }
    }
}

async fn fake_pga(mut stream: SerialStream) {
    loop {
        stream
            .write(&encode_player(&Player {
                pos: (100, 100),
                vel: (1, -2),
            }))
            .await
            .unwrap();

        tokio::time::sleep(Duration::from_millis(1000)).await;

        stream
            .write(&encode_player(&Player {
                pos: (200, 200),
                vel: (-3, 4),
            }))
            .await
            .unwrap();

        tokio::time::sleep(Duration::from_millis(1000)).await;
    }

    // leak the stream so it isn't dropped. dropping causes the other stream to close as well
    Box::leak(Box::new(stream));
}

fn decode_player(data: &[u8]) -> Player {
    macro_rules! unpack_array {
        ( $array:expr, [ $( $index:expr ),* $(,)? ], [$($padding:expr),* $(,)? ] ) => {
            [
                $($array[$index]),* ,
                $($padding),* ,
            ]
        };
    }

    let x = i32::from_be_bytes(unpack_array!(data, [2, 3, 4], [0x00])) >> 8;
    let y = i32::from_be_bytes(unpack_array!(data, [5, 6, 7], [0x00])) >> 8;
    let xvel = i32::from_be_bytes(unpack_array!(data, [8, 9, 10], [0x00])) >> 8;
    let yvel = i32::from_be_bytes(unpack_array!(data, [11, 12, 13], [0x00])) >> 8;

    Player {
        pos: (x, y),
        vel: (xvel, yvel),
    }
}

fn encode_player(player: &Player) -> [u8; 16] {
    let x_array = (player.pos.0).to_be_bytes();
    let y_array = (player.pos.1).to_be_bytes();
    let xvel_array = (player.vel.0).to_be_bytes();
    let yvel_array = (player.vel.1).to_be_bytes();

    // this doesn't match player.spade but it works.
    [
        0x01,
        0x01,
        x_array[1],
        x_array[2],
        x_array[3],
        y_array[1],
        y_array[2],
        y_array[3],
        xvel_array[1],
        xvel_array[2],
        xvel_array[3],
        yvel_array[1],
        yvel_array[2],
        yvel_array[3],
        0x00,
        0x00,
    ]
}

async fn accept_connection(
    stream: TcpStream,
    mut state_rx: watch::Receiver<Option<String>>,
    player_id: u8,
    input_tx: mpsc::Sender<Vec<u8>>,
) {
    let addr = stream
        .peer_addr()
        .expect("connected streams should have a peer address");
    info!("Peer address: {}", addr);

    let ws_stream = tokio_tungstenite::accept_async(stream)
        .await
        .expect("Error during the websocket handshake occurred");

    info!("New WebSocket connection: {}", addr);

    let (mut write, mut read) = ws_stream.split();
    let mut pinned = Pin::new(&mut write);

    // pinned
    //     .send(Message::Text(String::from("This is sent from rust")))
    //     .await
    //     .unwrap();

    loop {
        tokio::select! {
            Some(Ok(msg)) = read.next() => {
                match msg {
                    Message::Text(text) => {
                        match serde_json::from_str::<Input>(&text) {
                            Ok(decoded) => input_tx.send(
                                        vec![
                                        // Magic characters
                                        42,
                                        69,
                                        player_id,
                                        decoded.key,
                                        decoded.kind.byte_encoding()
                                    ]
                                ).await.unwrap(),
                            Err(e) => error!("Got invalid json from client {text} {e:?}")
                        }

                    }
                    _ => {}
                }
            }
            _ = state_rx.changed() => {
                let value = (*state_rx.borrow()).clone();
                if let Some(state) = value {
                    pinned.send(Message::Text(state)).await.unwrap()
                }
            }
        }
    }
}
