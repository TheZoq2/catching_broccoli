use std::time::Duration;

use libbroccoli::Player;
use log::info;
use reqwest::Client;
use serde::Deserialize;
use tokio::sync::watch::Receiver;

#[derive(Deserialize)]
struct PlayerResponse {
    id: u64,
}

pub async fn metaverse_manager(url: String, mut state_rx: Receiver<Option<String>>) -> ! {
    let client = Client::new();

    let res = client.put(format!("{url}/player"))
        .body("{}")
        .send()
        .await
        .unwrap();

    let body = res.text().await.unwrap();
    info!("Metaverse replied with {}", body);
    let id = serde_json::from_str::<PlayerResponse>(&body).expect("failed to decode json").id;

    info!("Metaverse id: {id}");

    loop {
        state_rx.changed().await.unwrap();

        let s = (*state_rx.borrow()).clone();
        match s {
            Some(state) => {
                let player = serde_json::from_str::<Player>(&state).unwrap();
                let msg = r#"{"position":{"x":$[x],"y":$[y],"z":0},"name":"Mr. Broccoli","id":$[id],"rotation":{"x":0.0,"y":0.0,"z":0.0},"velocity":{"x":$[xvel],"y":$[yvel],"z":0.0},"fishing":false}"#
                    .replace("$[id]", &format!("{}", id))
                    .replace("$[x]", &format!("{}", player.pos.0 as f32 / 100.))
                    .replace("$[y]", &format!("{}", -(player.pos.1 as f32 - 700.) / 100. + 1.))
                    .replace("$[xvel]", "0")
                    .replace("$[yvel]", "0");


                client.put(format!("{url}/player2"))
                    .body(msg)
                    .send()
                    .await
                    .unwrap();

                tokio::time::sleep(Duration::from_millis(100)).await
            },
            None => todo!(),
        }
    }
}
