use three_d::window::Event;
use three_d::*;

use wasm_bindgen::prelude::*;

use console_error_panic_hook;
use console_log;
use log::{error, info, Level};
use std::panic;
use std::rc::Rc;
use std::sync::Arc;
use std::{cell::RefCell, sync::Mutex};
#[cfg(target_arch = "wasm32")]
use wasm_bindgen::prelude::*;
use libbroccoli::{Input, InputKind};
use wasm_sockets::{self, Message, WebSocketError};

struct Player {
    drawable: Rectangle<ColorMaterial>,
    inner_player: libbroccoli::Player,
}

impl Player {
    fn new(context: &Context) -> Self {
        let x = 400;
        let y = 400;
        Self {
            drawable: Rectangle::new_with_material(
                context,
                vec2(x as f32, y as f32),
                degrees(0.0),
                35.0,
                100.0,
                ColorMaterial {
                    color: Color::RED,
                    ..Default::default()
                },
            )
            .unwrap(),
            inner_player: libbroccoli::Player {
                pos: (x, y),
                vel: (0, 0),
            },
        }
    }

    fn set_inner_player(&mut self, inner_player: libbroccoli::Player) {
        let new_center = vec2(inner_player.pos.0 as f32, inner_player.pos.1 as f32);
        self.inner_player = inner_player;
        self.drawable.set_center(new_center);
    }

    fn render(&self, viewport: Viewport) -> ThreeDResult<()> {
        self.drawable.render(viewport)
    }
}

fn convert_key(key: Key) -> u8 {
    match key {
        Key::A => b'A',
        Key::B => b'B',
        Key::C => b'C',
        Key::D => b'D',
        Key::E => b'E',
        Key::F => b'F',
        Key::G => b'G',
        Key::H => b'H',
        Key::I => b'I',
        Key::J => b'J',
        Key::K => b'K',
        Key::L => b'L',
        Key::M => b'M',
        Key::N => b'N',
        Key::O => b'O',
        Key::P => b'P',
        Key::Q => b'Q',
        Key::R => b'R',
        Key::S => b'S',
        Key::T => b'T',
        Key::U => b'U',
        Key::V => b'V',
        Key::W => b'W',
        Key::X => b'X',
        Key::Y => b'Y',
        Key::Z => b'Z',
        _ => b'_',
    }
}

fn main() -> Result<(), WebSocketError> {
    // console_log and log macros are used instead of println!
    // so that messages can be seen in the browser console

    panic::set_hook(Box::new(console_error_panic_hook::hook));
    // Create a window (a canvas on web)
    let window = Window::new(WindowSettings {
        title: "Triangle!".to_string(),
        max_size: Some((1280, 720)),
        ..Default::default()
    })
    .unwrap();

    // Get the graphics context from the window
    let context = window.gl().unwrap();

    let mut player = Arc::new(Mutex::new(Player::new(&context)));

    info!("Creating connection");

    let client = {
        let player = Arc::clone(&player);
        let mut client = wasm_sockets::EventClient::new("ws://81.233.119.252:8080")?;
        client.set_on_error(Some(Box::new(|error| {
            error!("{:#?}", error);
        })));
        client.set_on_connection(Some(Box::new(|client: &wasm_sockets::EventClient| {
            info!("{:#?}", client.status);
            info!("Sending message...");
            client.send_string("Hello, World!").unwrap();
            client.send_binary(vec![20]).unwrap();
        })));
        client.set_on_close(Some(Box::new(|| {
            info!("Connection closed");
        })));
        client.set_on_message(Some(Box::new(
            move |client: &wasm_sockets::EventClient, message: wasm_sockets::Message| {
                info!("New Message: {:#?}", message);
                if let Message::Text(message) = message {
                    let received_player: libbroccoli::Player =
                        serde_json::from_str(&message).unwrap();
                    player.lock().unwrap().set_inner_player(received_player);
                }
            },
        )));
        client
    };

    let mut camera = Camera::new_perspective(
        &context,
        window.viewport().unwrap(),
        vec3(1.0, 5.5, 10.0),
        vec3(0.0, 3.0, 0.0),
        vec3(0.0, 1.0, 0.0),
        degrees(60.0),
        0.1,
        1000.0,
    )
    .unwrap();

    let pipeline = ForwardPipeline::new(&context).unwrap();

    let mut box_object = Model::new_with_material(
        &context,
        &CPUMesh::cube(),
        ColorMaterial {
            ..Default::default()
        },
    ).unwrap();

    let lights = Lights {
        ambient: Some(AmbientLight {
            intensity: 0.4,
            color: Color::WHITE,
            ..Default::default()
        }),
        directional: vec![DirectionalLight::new(
            &context,
            2.0,
            Color::WHITE,
            &vec3(0.0, -1.0, -1.0),
        )
        .unwrap()],
        ..Default::default()
    };

    let objects = Loading::new(
        &context,
        &[
            "assets/Penguin.obj",
            "assets/Penguin.mtl",
            "assets/Ground.obj",
            "assets/Ground.mtl",
            "assets/Water.obj",
            "assets/Water.mtl"
        ],
        move |context, mut loaded| {
            let (meshes, materials) = loaded.obj("assets/Penguin.obj").unwrap();
            let mut penguin = Model::new_with_material(
                &context,
                &meshes[0],
                PhysicalMaterial::new(&context, &materials[0]).unwrap(),
            )
            .unwrap();
            penguin.material.opaque_render_states.cull = Cull::Back;

            let (meshes, materials) = loaded.obj("assets/Ground.obj").unwrap();
            let mut ground = Model::new_with_material(
                &context,
                &meshes[0],
                PhysicalMaterial::new(&context, &materials[0]).unwrap(),
            )
            .unwrap();
            ground.material.opaque_render_states.cull = Cull::Back;

            let (meshes, materials) = loaded.obj("assets/Water.obj").unwrap();
            let mut water = Model::new_with_material(
                &context,
                &meshes[0],
                PhysicalMaterial::new(&context, &materials[0]).unwrap(),
            )
            .unwrap();
            water.material.opaque_render_states.cull = Cull::Back;

            Ok((penguin, ground, water))
        },
    );

    // Start the main render loop
    window
        .render_loop(
            move |frame_input: FrameInput| // Begin a new frame with an updated frame input
    {
        if objects.is_loaded() {
            info!("Loaded penguin")
        }
        for event in frame_input.events {
            match event {
                Event::KeyPress{kind, modifiers, handled} => {
                    let msg = serde_json::to_string(&Input {
                        key: convert_key(kind),
                        kind: InputKind::Press
                    }).unwrap();
                    client.send_string(&msg);
                }
                Event::KeyRelease{kind, modifiers, handled} => {
                    let msg = serde_json::to_string(&Input {
                        key: convert_key(kind),
                        kind: InputKind::Release
                    }).unwrap();
                    client.send_string(&msg);
                }
                _ => {}
            }
        }
        // Start writing to the screen and clears the color and depth
        Screen::write(&context, ClearState::color_and_depth(0.7, 0.9, 1.0, 1.0, 1.0), || {
            let mut player = player.lock().unwrap();
            // player.drawable.set_rotation(radians((frame_input.accumulated_time * 0.005) as f32));
            player.render(frame_input.viewport)?;

            Ok(())
        }).unwrap();

        let player_pos = player.lock().unwrap().inner_player.pos;
        let player_transform = Mat4::from_translation(vec3(
            player_pos.0 as f32 / 100.,
            -(player_pos.1 as f32 -700.) / 100.,
            0.,
        ));
        if let Some(ref mut objects) = *objects.borrow_mut() {
            // let (penguin, ground, )
            let (ref mut penguin, ref ground, ref water) = objects.as_mut().unwrap();
            penguin.set_transformation(player_transform);
            pipeline.render_pass(
                &camera,
                &[&penguin, ground, water].as_slice(),
                &lights,
            ).unwrap();
        }
        else {
            box_object.set_transformation(player_transform);
            pipeline.render_pass(
                &camera,
                &[&box_object],
                &lights,
            ).unwrap();
        }

        // if let Some(ref objects) = *objects.borrow() {
        // let (box_object, penguin_object) = objects.as_ref().unwrap();

        // Returns default frame output to end the frame
        FrameOutput{swap_buffers: true, .. Default::default()}
    },
        )
        .unwrap();

    Ok(())
}

#[wasm_bindgen(start)]
pub fn start() -> Result<(), JsValue> {
    console_log::init_with_level(log::Level::Debug).unwrap();

    use log::info;
    info!("Logging works!");

    std::panic::set_hook(Box::new(console_error_panic_hook::hook));
    main().unwrap();
    Ok(())
}
